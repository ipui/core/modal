import React from 'react';
import './modal.sass';

function Modal( props ) {
  function defaultAction() {
    console.info( 'modal close function is no defined' );
  }

  const {
    layout = '',
    closeWith = defaultAction
  } = props;

  function handleClose( event ) {
    const { id } = event.target
    if ( id !== 'modal' )
      return

    closeWith();
  }

  const classes = [
    ...( layout.split( ' ' ) )
  ].join( ' ' )

  return (
    <div
      id="modal"
      className={ classes }
      onClick={ handleClose }
    >
      <div className="window">
        { props.children }
      </div>
    </div>
  );
}

export default Modal;
